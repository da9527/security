package com.securite.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author da
 * @Date 2021/10/24 上午 10:51
 */
@Controller
public class LoginController
{

    @RequestMapping("/toMain")
    public String main()
    {
        return "redirect:main.html";
    }

    @RequestMapping("/toError")
    public String error()
    {
        return "redirect:error.html";
    }
}
