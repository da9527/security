package com.securite.security.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description: TODO(自定义登陆成功处理器)
 * @Author da
 * @Date 2021/10/24 上午 11:49
 */
public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler
{

    private final String url;

    public MyAuthenticationSuccessHandler(String url)
    {
        this.url = url;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException
    {

//        获取登陆的用户
        User user = (User) authentication.getPrincipal();
//        密码会为null
        System.out.println("username : " + user.getUsername() + " password : " + user.getPassword() + "权限 : " + user.getAuthorities());

//        重定向到传入的url
        httpServletResponse.sendRedirect(url);
    }
}
